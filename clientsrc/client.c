/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/23 17:01:28 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf_bonus.h"
#include <limits.h>
#include <signal.h>
#include <stdlib.h>

int	g_send;

static size_t	ft_ustrlen(const unsigned char *s)
{
	size_t	i;

	i = 0;
	while (s && s[i] != '\0')
		i++;
	return (i);
}

static void	input(int signal, siginfo_t *info, void *uap)
{
	(void)info;
	(void)uap;
	if (signal == 30)
		g_send = 1;
	else
	{
		ft_printf("Communication successful.\n");
		exit(EXIT_SUCCESS);
	}
}

static void	send(int content, int pid, int len, unsigned int mask)
{
	int	timer;

	timer = INT_MAX;
	while (len)
	{
		if (g_send == 1)
		{
			g_send = 0;
			timer = INT_MAX;
			len--;
			kill(pid, ((content & mask) >> len) + 30);
			mask >>= 1;
		}
		else
		{
			timer--;
			if (!timer)
			{
				ft_printf("Timeout.\n");
				exit(EXIT_FAILURE);
			}
		}
	}
}

static void	sendmessage(unsigned char *message, int pid)
{
	int	i;

	i = 0;
	send(ft_ustrlen(message) + 2, pid, 32, 0x80000000);
	while (message[i])
	{
		send(message[i], pid, 8, 0x80);
		i++;
	}
	send('\n', pid, 8, 0x80);
	send('\0', pid, 8, 0x80);
}

int	main(int argc, char **argv)
{
	struct sigaction	act;

	act.sa_sigaction = input;
	sigaction(30, &act, NULL);
	sigaction(31, &act, NULL);
	g_send = 1;
	if (argc < 3)
	{
		ft_printf("Not enough arguments.\n");
		return (1);
	}
	else if (ft_atoi(argv[1]) < 1)
	{
		ft_printf("Invalid PID.\n");
		return (1);
	}
	else
		sendmessage((unsigned char *)argv[2], ft_atoi(argv[1]));
	while (1)
		(void)0;
	return (0);
}
