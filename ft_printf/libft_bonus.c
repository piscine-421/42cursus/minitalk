/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:29 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

int	ft_atoi(const char *str)
{
	int	i;
	int	sign;
	int	returned;

	i = 0;
	returned = 0;
	if (!str)
		return (returned);
	sign = 1;
	while ((str[i] > 8 && str[i] < 14) || str[i] == 32)
		i++;
	if (str[i] == 43 || str[i] == 45)
	{
		if (str[i] == 45)
			sign = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		returned *= 10;
		returned += str[i] - 48;
		i++;
	}
	returned *= sign;
	return (returned);
}

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int	ft_putchar_fd(char c, int l, int p, t_flag *f)
{
	f->w -= l;
	f->p -= p;
	f->r++;
	return (write(1, &c, 1));
}

int	ft_putnbr_base(unsigned long nbr, char *b, t_flag *f)
{
	unsigned long	t[3];

	t[0] = 1;
	t[2] = 0;
	while (b[t[2]])
		t[2]++;
	t[1] = nbr;
	while (t[1] >= t[2] && t[0]++)
		t[1] /= t[2];
	t[1] = 1;
	while (t[0]-- > 1 && t[2] > 1)
		t[1] *= t[2];
	while (t[1])
	{
		if (ft_putchar_fd(b[nbr / t[1] % t[2]], 1, 1, f) == -1)
			return (-1);
		t[1] /= t[2];
	}
	return (0);
}

int	ft_putnbr_fd(unsigned int n, t_flag *f)
{
	char		nbc;
	int			divisor;
	int			start;

	start = 0;
	divisor = 1000000000;
	if (n == 0 && ft_putchar_fd('0', 1, 1, f) == -1)
		return (-1);
	while (divisor >= 1 && n != 0)
	{
		nbc = n / divisor % 10 + 48;
		divisor /= 10;
		if (nbc != '0')
			start = 1;
		if (start == 1 && ft_putchar_fd(nbc, 1, 1, f) == -1)
			return (-1);
	}
	return (0);
}
