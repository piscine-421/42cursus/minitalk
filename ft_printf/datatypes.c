/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   datatypes.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:03:59 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	prtchr(char c, int *r)
{
	return (ft_putchar_fd(c, 1, r));
}

int	prtstr(char *s, int *r)
{
	if (!s)
		s = "(null)";
	return (ft_putstr_fd(s, 1, r));
}

int	prtptr(unsigned long n, int *r)
{
	if (ft_putstr_fd("0x", 1, r) == -1)
		return (-1);
	return (ft_putnbr_base(n, "0123456789abcdef", r));
}

int	prtnbr(long n, int *r)
{
	if (n < 0)
	{
		if (ft_putchar_fd('-', 1, r) == -1)
			return (-1);
		n *= -1;
	}
	return (ft_putnbr_fd(n, 1, r));
}

int	prtunbr(unsigned int n, int *r)
{
	return (ft_putnbr_fd(n, 1, r));
}
