/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   datatypes_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:03:58 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

int	g(long n, int base, t_flag *f)
{
	int	s;

	s = 1;
	if (!n && f->p < 0)
		s++;
	while (n)
	{
		n /= base;
		s++;
	}
	return (s);
}

int	gu(unsigned long n, int base, t_flag *f)
{
	int	s;

	s = 1;
	if (!n && f->p < 0)
		s++;
	while (n)
	{
		n /= base;
		s++;
	}
	return (s);
}

int	prtchr(char c, t_flag *f)
{
	f->w--;
	while (!f->d && !f->z && f->w >= 1)
		if (ft_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	while (!f->d && f->z && f->w >= 1)
		if (ft_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if (ft_putchar_fd(c, 1, 1, f) == -1)
		return (-1);
	f->w++;
	while (f->d && f->w > 0)
		if (ft_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}

int	prtstr(char *s, t_flag *f)
{
	int	len;

	if (!s)
		s = "(null)";
	len = ft_strlen(s);
	if (f->p >= 0 && f->p < len)
		len = f->p;
	if (f->h && f->w < 0)
		f->w = 0;
	while (!f->d && f->w > len)
		if (ft_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	if (ft_putstr_fd(s, 1, 1, f) == -1)
		return (-1);
	while (f->w > 0)
		if (ft_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}

int	prtptr(unsigned long n, t_flag *f)
{
	f->w -= 2;
	while (!f->d && (!f->z || f->p >= 0) && f->w >= gu(n, 16, f) && f->w > f->p)
		if (ft_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	if (ft_putchar_fd('0', 0, 0, f) == -1)
		return (-1);
	if (ft_putchar_fd('x', 0, 0, f) == -1)
		return (-1);
	while (f->p >= gu(n, 16, f) || (!f->d && f->z && f->w >= gu(n, 16, f)))
		if (ft_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if ((n || f->p < 0) && ft_putnbr_base(n, "0123456789abcdef", f) == -1)
		return (-1);
	while (f->d && f->w > 0)
		if (ft_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}
