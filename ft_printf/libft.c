/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:32 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putchar_fd(char c, int fd, int *r)
{
	*r += 1;
	return (write(fd, &c, 1));
}

int	ft_putnbr_base(unsigned long nbr, char *b, int *r)
{
	unsigned long	t[3];

	t[0] = 1;
	t[2] = 0;
	while (b[t[2]])
		t[2]++;
	t[1] = nbr;
	while (t[1] >= t[2] && t[0]++)
		t[1] /= t[2];
	t[1] = 1;
	while (t[0]-- > 1 && t[2] > 1)
		t[1] *= t[2];
	while (t[1])
	{
		if (ft_putchar_fd(b[nbr / t[1] % t[2]], 1, r) == -1)
			return (-1);
		t[1] /= t[2];
	}
	return (0);
}

int	ft_putnbr_fd(unsigned int n, int fd, int *r)
{
	char		nbc;
	int			divisor;
	int			start;

	start = 0;
	divisor = 1000000000;
	if (n == 0 && ft_putchar_fd('0', fd, r) == -1)
		return (-1);
	while (divisor >= 1 && n != 0)
	{
		nbc = n / divisor % 10 + 48;
		divisor /= 10;
		if (nbc != '0')
			start = 1;
		if (start == 1 && ft_putchar_fd(nbc, fd, r) == -1)
			return (-1);
	}
	return (0);
}

int	ft_putstr_fd(char *s, int fd, int *r)
{
	int	i;
	int	len;

	i = 0;
	len = ft_strlen(s);
	while (s[i] && i < len)
	{
		if (ft_putchar_fd(s[i], fd, r) == -1)
			return (-1);
		i++;
	}
	return (0);
}

long	ft_strlen(const char *s)
{
	int	i;

	i = 0;
	while (s && s[i] != '\0')
		i++;
	return (i);
}
