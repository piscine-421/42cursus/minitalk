LIBS = ft_printf
FLAGS = -Wall -Wextra -Werror -I$(LIBS) -L$(LIBS) -lftprintf
CNAME = client
SNAME = server
CSRC = client.c
SSRC = get_next_line_bonus.c get_next_line_utils_bonus.c server.c

all : $(CNAME) $(SNAME)

bonus : all

clean:
	@make clean -s -C $(LIBS)

fclean:
	@make fclean -s -C $(LIBS)
	rm -rf $(CNAME) $(SNAME)

$(CNAME) : $(addprefix $(CNAME)src/,$(CSRC))
	@make bonus -s -C $(LIBS)
	cc $(FLAGS) $(addprefix $(CNAME)src/,$(CSRC)) -o $(CNAME)

$(SNAME) : $(addprefix $(SNAME)src/,$(SSRC))
	@make bonus -s -C $(LIBS)
	cc $(FLAGS) $(addprefix $(SNAME)src/,$(SSRC)) -o $(SNAME)

re : fclean all
