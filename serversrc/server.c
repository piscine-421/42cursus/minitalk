/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/23 17:02:58 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf_bonus.h"
#include "get_next_line_bonus.h"
#include <signal.h>

int	main(void);

static int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (!s1 || !s2 || !n)
		return (0);
	while (s1[i] == s2[i])
	{
		n--;
		if (!s1[i] || !n)
			return (0);
		i++;
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}

static char	*printit(char *s, int pid)
{
	ft_printf("%s", s);
	free(s);
	s = 0;
	kill(pid, 31);
	return (s);
}

static void	inputcopy(char *print, int *charb, int pid, int *size)
{
	static int	i = 0;

	print[i++] = *charb;
	*charb = 0;
	if (i == *size)
	{
		print = printit(print, pid);
		i = 0;
		*size = 0;
	}
}

static void	input(int signal, siginfo_t *info, void *uap)
{
	static int		charb = 0;
	static int		i = 0;
	static pid_t	pid = 0;
	static char		*print = 0;
	static int		size = 0;

	(void)uap;
	if (info->si_pid)
		pid = info->si_pid;
	charb = charb << 1 ^ signal - 30;
	i++;
	if (!size && i == 32)
	{
		size = charb;
		charb = 0;
		print = malloc(size);
		i = 0;
	}
	else if (size && i == 8)
	{
		i = 0;
		inputcopy(print, &charb, pid, &size);
	}
	kill(pid, 30);
}

int	main(void)
{
	struct sigaction	act;
	char				*message;

	act.sa_sigaction = input;
	act.sa_flags = SA_SIGINFO;
	ft_printf("pid: %d\n", getpid());
	sigaction(30, &act, NULL);
	sigaction(31, &act, NULL);
	while (1)
	{
		message = get_next_line(1);
		if (!ft_strncmp(message, "exit", 4) && ft_strlen(message) == 5)
			break ;
		free(message);
	}
	free(message);
	return (0);
}
